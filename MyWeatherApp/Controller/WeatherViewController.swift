//
//  WeatherViewController.swift
//  MyWeatherApp
//
//  Created by Muqeem Ahmad on 01/03/21.
//

import UIKit
import MapKit

class WeatherViewController: UIViewController {

    @IBOutlet weak var rightNowView: RightNowView!
    @IBOutlet weak var weatherDetailView: WeatherDetailView!
    
    var city = ""
    var weatherResult: Result?
    var coordinate: CLLocationCoordinate2D? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        clearAll()

        if weatherResult != nil {
            NetworkService.shared.setLatitude(weatherResult!.lat)
            NetworkService.shared.setLongitude(weatherResult!.lon)
            
            self.updateViews()
        } else {
            NetworkService.shared.setLatitude(coordinate!.latitude)
            NetworkService.shared.setLongitude(coordinate!.longitude)
            
            getWeather()
        }
    }
    
    func clearAll() {
        rightNowView.clear()
        weatherDetailView.clear()
    }
    
    func getWeather() {
        NetworkService.shared.getWeather(onSuccess: { (result) in
            self.weatherResult = result
            
            self.weatherResult?.sortDailyArray()
            self.weatherResult?.sortHourlyArray()
            
            self.updateViews()
            
        }) { (errorMessage) in
            debugPrint(errorMessage)
        }
    }
    
    func updateViews() {
        updateTopView()
        updateBottomView()
    }
    
    func updateTopView() {
        guard let weatherResult = weatherResult else {
            return
        }
        
        rightNowView.updateView(currentWeather: weatherResult.current, city: city)
    }
    
    func updateBottomView() {
        guard let weatherResult = weatherResult else {
            return
        }
        
        let title = weatherDetailView.getSelectedTitle()
        
        if title == "Today" {
            weatherDetailView.updateViewForToday(weatherResult.hourly)
        } else if title == "Weekly" {
            weatherDetailView.updateViewForWeekly(weatherResult.daily)
        }
        
        
        var theJSONText: String = ""
        let dictionary = weatherResult.dictionary
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: dictionary as Any,
            options: []) {
            theJSONText = String(data: theJSONData,
                                 encoding: .ascii)!
            print("JSON string = \(theJSONText)")
        }
        
        let dict = ["city": city,
                    "weather": theJSONText
        ] as [String : String]
        DatabaseHelper.shareInstance.updateCurrentWeather(object: dict, city: city)
        
        
        
    }

    @IBAction func onDoneClick(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController:HomeScreenViewController = storyboard.instantiateViewController(withIdentifier: "HomeScreenViewController") as! HomeScreenViewController
        self.present(viewController, animated: true, completion: nil)
    }
    
    @IBAction func getWeatherTapped(_ sender: UIButton) {
        clearAll()
        getWeather()
    }
    
    @IBAction func todayWeeklyValueChanged(_ sender: UISegmentedControl) {
        clearAll()
        updateViews()
    }
}

extension Encodable {

  var dictionary: [String: Any]? {
    guard let data = try? JSONEncoder().encode(self) else { return nil }
    return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
  }

}
