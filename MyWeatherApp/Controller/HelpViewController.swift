//
//  HelpViewController.swift
//  MyWeatherApp
//
//  Created by Muqeem Ahmad on 03/03/21.
//

import UIKit
import WebKit

class HelpViewController: UIViewController {
    @IBOutlet weak var webView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let htmlPath = Bundle.main.path(forResource: "help", ofType: "html")

         let url = URL(fileURLWithPath: htmlPath!)

         let request = URLRequest(url: url)
         webView.load(request)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func back(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
