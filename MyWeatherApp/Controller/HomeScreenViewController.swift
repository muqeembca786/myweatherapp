//
//  HomeScreenViewController.swift
//  MyWeatherApp
//
//  Created by Muqeem Ahmad on 02/03/21.
//

import UIKit

class HomeScreenViewController: UIViewController {

    
    var weatherList: [WeatherEntity] = []
//    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        tableView.register(UINib(nibName: "HomeCell", bundle: nil), forCellReuseIdentifier: "HomeCell")
        
        weatherList = DatabaseHelper.shareInstance.getAllWeather()
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func onHelpClick(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController:HelpViewController = storyboard.instantiateViewController(withIdentifier: "HelpViewController") as! HelpViewController
        self.present(viewController, animated: true, completion: nil)
    }
    
    @IBAction func searchHandler(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController:AddLocationViewController = storyboard.instantiateViewController(withIdentifier: "AddLocationViewController") as! AddLocationViewController
        self.present(viewController, animated: true, completion: nil)
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
}

extension HomeScreenViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weatherList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let weather = weatherList[indexPath.row]
        let weatherInString = weather.weather
        var weatherResult: Result? = nil
        do{
            weatherResult = try JSONDecoder().decode(Result.self, from: (weatherInString?.data(using: .utf8))!)
            print("temp:",weatherResult!.current.temp)
        }catch let jsonErr {
            
            print(jsonErr)
        }

        let date = Date(timeIntervalSince1970: Double((weatherResult?.current.dt)!))
        let hourString = Date.getHourFrom(date: date)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "homeCell") as! HomeCell
        
        
        cell.timeLabel.text = hourString
        cell.cityLabel.text = weather.city
        
        let weatherTemperature = weatherResult?.current.temp
        cell.tempLabel.text = "\(Int(weatherTemperature!.rounded()))°F"
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        return cell
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let weather = weatherList[indexPath.row]
            DatabaseHelper.shareInstance.deleteCity(city: weather.city!)
            weatherList.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let weather = weatherList[indexPath.row]
        let weatherInString = weather.weather
        var weatherResult: Result? = nil
        do{
            weatherResult = try JSONDecoder().decode(Result.self, from: (weatherInString?.data(using: .utf8))!)
            print("temp:",weatherResult!.current.temp)
        }catch let jsonErr {
            
            print(jsonErr)
        }

        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController:WeatherViewController = storyboard.instantiateViewController(withIdentifier: "WeatherViewController") as! WeatherViewController
        viewController.weatherResult = weatherResult
        viewController.city = weather.city!
        self.present(viewController, animated: true, completion: nil)
    }
}
