//
//  DatabaseHelper.swift
//  MyWeatherApp
//
//  Created by Muqeem Ahmad on 02/03/21.
//
//

import Foundation
import CoreData
import UIKit

class DatabaseHelper{
    static var shareInstance = DatabaseHelper()

    let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext


    
    //Save new city weather
    func saveCurrentWeather(object:[String:String]){
        let weatherEntity = NSEntityDescription.insertNewObject(forEntityName: "WeatherEntity", into: context!) as! WeatherEntity
        weatherEntity.city = object["city"]
        weatherEntity.latitude = object["latitude"]
        weatherEntity.longitude = object["longitude"]
        weatherEntity.weather = object["weather"]
        weatherEntity.temperature = object["temperature"]
        do{
            try context?.save()
        }catch{
            print("data is not save")
        }
    }
    
    //Get existing city weather
    func getCityWeather(city:String) -> [WeatherEntity]{
        var weatherEntity = [WeatherEntity]()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "WeatherEntity")
        fetchRequest.predicate = NSPredicate(format: "city == %@", city)
        do{
            weatherEntity = try context?.fetch(fetchRequest) as! [WeatherEntity]
        }catch{
            print("can not get data")
        }
        return weatherEntity
    }
    
    //Get all weather
    func getAllWeather() -> [WeatherEntity]{
        var weatherEntity = [WeatherEntity]()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "WeatherEntity")
        do{
            weatherEntity = try context?.fetch(fetchRequest) as! [WeatherEntity]
        }catch{
            print("can not get data")
        }
        return weatherEntity
    }
    
    //Update weather
    func updateCurrentWeather(object:[String:String],city:String){
        let weatherEntity = getCityWeather(city: city)
        if weatherEntity.count > 0 {
            weatherEntity[0].city = object["city"]
            weatherEntity[0].latitude = object["latitude"]
            weatherEntity[0].longitude = object["longitude"]
            weatherEntity[0].weather = object["weather"]
            weatherEntity[0].temperature = object["temperature"]
        } else {
            saveCurrentWeather(object: object)
        }

        do{
            try context?.save()
        }catch{
            print("data is not update")
        }
    }
    
    
    func deleteCity(city:String) {
        let weatherList = getAllWeather()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "WeatherEntity")
        fetchRequest.predicate = NSPredicate(format: "city == %@", city)
        
        for weather in weatherList {
            if city == weather.city {
                context?.delete(weather)
                do{
                    try context?.save()
                }catch{
                    print("data is not save")
                }
            }
        }
        
        
    }
    func deleteAllData(_ entity:String) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        fetchRequest.returnsObjectsAsFaults = false
        do {
            let results = try context?.fetch(fetchRequest)
            for object in results! {
                guard let objectData = object as? NSManagedObject else {continue}
                context?.delete(objectData)
            }
        } catch let error {
            print("Detele all data in \(entity) error :", error)
        }
    }

}
