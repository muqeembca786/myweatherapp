//
//  WeatherEntity+CoreDataProperties.swift
//  MyWeatherApp
//
//  Created by Muqeem Ahmad on 02/03/21.
//
//

import Foundation
import CoreData


extension WeatherEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<WeatherEntity> {
        return NSFetchRequest<WeatherEntity>(entityName: "WeatherEntity")
    }

    @NSManaged public var city: String?
    @NSManaged public var latitude: String?
    @NSManaged public var longitude: String?
    @NSManaged public var temperature: String?
    @NSManaged public var weather: String?

}

